<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <h5 class="my-0 mr-md-auto font-weight-normal"><img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 70px; width: 70px; display: block;" src="https://scontent.fbkk22-3.fna.fbcdn.net/v/t1.15752-9/98201865_656273281590722_1452340605572087808_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_eui2=AeECd1aU5XJSfWYtSCyZrYYC1dZ6g57c-S_V1nqDntz5L5TUJpJZvzB7h_rEC3C9xOwT6V7hKqY5Kb9yvngFuY1t&_nc_ohc=Rwuen0BjDjUAX-fl2gl&_nc_ht=scontent.fbkk22-3.fna&oh=d57f25d6e54db2b10e83b769a49f0127&oe=5EEBF54E" data-holder-rendered="true"></h5>

            <nav class="my-2 my-md-0 mr-md-3">


            </nav>
            <form method="real" action="<?php echo e(url('/home')); ?>">
                <?php echo csrf_field(); ?>
                <button type="submit" class="btn btn-outline-secondary">HOME</button>
            </form>
            <form method="real" action="<?php echo e(url('/new')); ?>">
                <?php echo csrf_field(); ?>
                <button type="submit" class="btn btn-outline-secondary">CREATE MOVIE</button>
            </form>
            <form method="real" action="<?php echo e(url('/')); ?>">
                <?php echo csrf_field(); ?>
                <button type="submit" class="btn btn-outline-secondary">MANAGE DATA</button>
            </form>

        </div>
        <div class="d-flex flex-row">
            <div style="font-size: xx-large" class="p-2">MANAGE</div>
        </div>
    <a href="<?php echo e(url('/')); ?>" class="btn-secondary"> Go back</a>
        
        <table class="table table-striped">
            <thead>
            <tr class="">
                <th scope="col">รหัส</th>
                <th scope="col">รูป</th>
                <th scope="col">ชื่อหนัง</th>
                <th scope="col">ประเภท</th>
                <th scope="col">แก้ไขข้อมูล</th>
                <th scope="col">ลบข้อมูล</th>
            </tr>
            </thead>
                    <tr>
                        <td>
                            <h1><?php echo e($todo->title); ?></h1>
                        </td>

                        <td>


                                <p class="text-dark"></p>
                            <img src="<?php echo e(url('uploads/'.$todo->file_name)); ?>"width="50">
                        </td>
                        <td>

                            <p><?php echo e($todo->content); ?></p>
                        </td>
                        <td>
                            <p>Action Movies</p>
                        </td>

                        <td>
                            <a href="<?php echo e(url('/todo/'.$todo->id.'/edit')); ?>" class="btn btn-primary">Edit</a>
                        </td>

                        <td>
                            <form action="<?php echo e(url('/todo/'.$todo->id)); ?>" method="post" id="form-delete">
                                <?php echo method_field('DELETE'); ?>
                                <?php echo csrf_field(); ?>
                                <button class="btn btn-danger" onclick="confirm_delete()" type="button">Delete</button>
                            </form>
                        </td>
                    </tr>
            </tbody>
        </table>
















        




    <script>
        function confirm_delete() {
            var text = '<?php echo $todo->title; ?>';
            var confirm = window.confirm('ยืนยันการลบนี้'+text);
            if (confirm) {
                document.getElementById('form-delete').submit();
            }
        }
    </script>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/M-TIT-LAR/resources/views/show.blade.php ENDPATH**/ ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <h5 class="my-0 mr-md-auto font-weight-normal"><img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 70px; width: 70px; display: block;" src="https://scontent.fbkk22-3.fna.fbcdn.net/v/t1.15752-9/98201865_656273281590722_1452340605572087808_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_eui2=AeECd1aU5XJSfWYtSCyZrYYC1dZ6g57c-S_V1nqDntz5L5TUJpJZvzB7h_rEC3C9xOwT6V7hKqY5Kb9yvngFuY1t&_nc_ohc=Rwuen0BjDjUAX-fl2gl&_nc_ht=scontent.fbkk22-3.fna&oh=d57f25d6e54db2b10e83b769a49f0127&oe=5EEBF54E" data-holder-rendered="true"></h5>

            <nav class="my-2 my-md-0 mr-md-3">


            </nav>
            <form method="real" action="{{url('/home')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary">HOME</button>
            </form>
            <form method="real" action="{{url('/todo/create')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary">CREATE MOVIE</button>
            </form>
            <form method="real" action="{{url('/')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary">MANAGE DATA</button>
            </form>
            {{--            <a class="btn btn-outline-primary" href="#">Sign up</a>--}}
        </div>
        <div style="text-align: center">
            <button type="submit" class="btn btn-outline-secondary">ALL</button>
            <button type="submit" class="btn btn-outline-secondary">Drama</button>
            <button type="submit" class="btn btn-outline-secondary">Sci-fi</button>
            <button type="submit" class="btn btn-outline-secondary">Family</button>
            <button type="submit" class="btn btn-outline-secondary">Thriller</button>
            <button type="submit" class="btn btn-outline-secondary">Crime</button>
            <button type="submit" class="btn btn-outline-secondary">Documentaries</button>
            <button type="submit" class="btn btn-outline-secondary">Animation</button>
        </div>


        <main role="main">


            <div class="album py-5">
                <div class="container">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style=" display: block;" src="https://movie-online.org/wp-content/uploads/2020/05/INSURGENT-2015-%E0%B8%84%E0%B8%99%E0%B8%81%E0%B8%9A%E0%B8%8F%E0%B9%82%E0%B8%A5%E0%B8%81.jpg" data-holder-rendered="true">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Bloodshot.jpg" data-holder-rendered="true" style="display: block;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/04/tumblr_c215dfa8a9d90e48636d346627c6ccc7_145ca6b6_1280.jpg" data-holder-rendered="true" style="display: block;">

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Maleficent.jpg" data-holder-rendered="true" style="display: block;">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Acceleration.jpg" data-holder-rendered="true" style="display: block;">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Wonder-Woman.jpg" data-holder-rendered="true" style="display: block;">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Iron-Man-3.jpg" data-holder-rendered="true" style="display: block;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Glass.jpg" data-holder-rendered="true" style="display: block;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="https://www.newmovie-hd.org/wp-content/uploads/2020/05/Men-in-Black-International.jpg" data-holder-rendered="true" style="display: block;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <h5 class="my-0 mr-md-auto font-weight-normal"><img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 70px; width: 70px; display: block;" src="https://scontent.fbkk22-3.fna.fbcdn.net/v/t1.15752-9/98201865_656273281590722_1452340605572087808_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_eui2=AeECd1aU5XJSfWYtSCyZrYYC1dZ6g57c-S_V1nqDntz5L5TUJpJZvzB7h_rEC3C9xOwT6V7hKqY5Kb9yvngFuY1t&_nc_ohc=Rwuen0BjDjUAX-fl2gl&_nc_ht=scontent.fbkk22-3.fna&oh=d57f25d6e54db2b10e83b769a49f0127&oe=5EEBF54E" data-holder-rendered="true"></h5>

            <nav class="my-2 my-md-0 mr-md-3">


            </nav>
            <form method="real" action="{{url('/home')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary">HOME</button>
            </form>
            <form method="real" action="{{url('/todo/create')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary">CREATE MOVIE</button>
            </form>
            <form method="real" action="{{url('/')}}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary">MANAGE DATA</button>
            </form>

        </div>
        <div class="d-flex flex-row">
            <div style="font-size: xx-large" class="p-2">Edit</div>
        </div>

    <form method="post" action="{{url('/todo/'.$todo->id)}}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>ชื่อหนัง</label>
            <input type="text" name="title" class="form-control" value="{{ $todo->title }}">
        </div>
        <div class="form-group">
            <label>ประเภท</label>
            <input type="text" name="content" class="form-control" value="{{ $todo->content }}">
        </div>
        <div class="form-group">
            <label>เวลา</label>
            <input type="date" name="due" class="form-control" value="{{ $todo->due }}">
        </div>
        <button type="submit">submit</button>
    </form>
    </div>
@endsection
